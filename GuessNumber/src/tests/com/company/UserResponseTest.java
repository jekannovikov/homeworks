package tests.com.company;

import main.com.company.UserResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class UserResponseTest {
    UserResponse cut = new UserResponse();

    static Arguments[] getUserNumTestArgs(){
        return new Arguments[]{
                Arguments.arguments("10", 10),
                Arguments.arguments("asff", 0),
                Arguments.arguments("1234", 0),
                Arguments.arguments("0", 0),
                Arguments.arguments("100", 100),
                Arguments.arguments("101", 0),
                Arguments.arguments("-1", 0),
                Arguments.arguments("asd", 0),
                Arguments.arguments("/][.,", 0),
        };
    }
    @ParameterizedTest
    @MethodSource("getUserNumTestArgs")
    void getUserNumTest(String userResponse, int expected){
        int actual = cut.getUserNum(userResponse);
        Assertions.assertEquals(actual, expected);

    }
}

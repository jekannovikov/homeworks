package tests.com.company;

import main.com.company.HotOrCold;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class HotOrColdTest {
    HotOrCold cut = new HotOrCold();
    static Arguments[] isHotOrColdTestArgs(){
        return new Arguments[]{
                Arguments.arguments(1, 2, "Colder"),
                Arguments.arguments(2, 1, "Hotter"),
                Arguments.arguments(2, 2, "You have guessed it!"),
                Arguments.arguments(100, 100, "You have guessed it!"),
                Arguments.arguments(1, 1, "You have guessed it!"),
        };
    }
    @ParameterizedTest
    @MethodSource("isHotOrColdTestArgs")
    void isHotOrColdTest(int old, int current, String expected){
        String actual = cut.isHotOrCold(old, current);
        Assertions.assertEquals(actual, expected);
    }
}

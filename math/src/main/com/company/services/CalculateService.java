package main.com.company.services;


public class CalculateService {

 private static final double FREE_FALL_ACCELERATION = 9.80665;



 public double getLengthOfGaubicaShootingDegree(double primarySpeed, double angle){
  double result = (primarySpeed * primarySpeed) * Math.sin(Math.toRadians(2 * angle)) / FREE_FALL_ACCELERATION;
  return result;
 }
 public static double getLengthOfGaubicaShootingRadians(double primarySpeed, double angle) {

  double result = ((primarySpeed * primarySpeed) * Math.sin(2 * angle)) / FREE_FALL_ACCELERATION;

  return result;

 }

 public static double getLengthBetweenCars( double firstSpeed,
                                           double secondSpeed, double distanceBetween, double time){

  double result = (firstSpeed * time) + (secondSpeed * time) + distanceBetween;

  return result;
 }

 public int getIsPointInFigure(double x, double y) {
      int PointInArea;
      if (((x >= 0) && (y >= 1.5 * x - 1) && (y <= x)) || ((x <= 0) && (y >= -1.5 * x - 1) && (y <= -x))) {
        PointInArea = 1;
  } else {
        PointInArea = 0;
  }
  return PointInArea;
 }


 public static double getResultOfBigFormula(double x) {

  double result = (6 * Math.log(Math.sqrt(Math.exp(x + 1) + 2 * Math.exp(x) * Math.cos(x)))) / (Math.log(x -
          (Math.exp(x + 1)) * Math.sin(x))) + Math.abs(Math.cos(x) / Math.exp(Math.sin(x)));

 return result;
 }
}


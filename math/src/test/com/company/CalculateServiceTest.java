package test.com.company;


import main.com.company.services.CalculateService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static java.lang.Float.NaN;

class CalculateServiceTest {



    CalculateService cut = new CalculateService();

    static Arguments[] getLengthOfGaubicaShootingDegreeTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3.2 , 57.17, 0.95137, 0.00001),
                Arguments.arguments(4.1 , 45.2, 1.7141, 0.00001)
        };
    }

    static Arguments[] getLengthOfGaubicaShootingRadiansTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3.2, 1.0, 0.94947, 0.00001),
                Arguments.arguments(4.1, 0.6, 1.59765, 0.00001)
        };
    }

    static Arguments[] getLengthBetweenCarsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(10.0, 10.0, 50.0, 3.0, 110.0, 0.00001),
                Arguments.arguments(5.0, 15.0, 110.5, 8.0, 270.5, 0.00001)
        };
    }
        static Arguments[] getIsPointInFigureTestArgs(){
            return new Arguments[]{
                    Arguments.arguments(3.0, 2.0, 0),
                    Arguments.arguments(-0.5, 0.5, 1),
                    Arguments.arguments(0.0, -1.0, 1),
                    Arguments.arguments(2.0, 2.0, 1),
                    Arguments.arguments(-3.0, 0.0, 0)
            };

    }
        static Arguments[] getResultOfBigFormulaTestArgs(){
            return new Arguments[]{
                    Arguments.arguments(5.5, 4.8034, 0.00001),
                    Arguments.arguments(1.0, NaN, 0.00001),
                    Arguments.arguments(11.0, 3.012828, 0.00001)
            };
        }
  @ParameterizedTest
  @MethodSource("getLengthOfGaubicaShootingDegreeTestArgs")
  void getFlightDistanceDegreeTest(double primarySpeed, double angle, double expected, double error){
      double actual = cut.getLengthOfGaubicaShootingDegree(primarySpeed, angle);
      Assertions.assertEquals(expected, actual, error);
  }

    @ParameterizedTest
    @MethodSource("getLengthOfGaubicaShootingRadiansTestArgs")
    void getFlightDistanceRadTest(double primarySpeed, double angle, double expected, double error){
        double actual = cut.getLengthOfGaubicaShootingRadians(primarySpeed, angle);
        Assertions.assertEquals(expected, actual, error);
    }

    @ParameterizedTest
    @MethodSource("getLengthBetweenCarsTestArgs")
    void getDistanceBtwCarsTest(double firstSpeed, double secondSpeed, double distanceBetween, double time,
                                double expected, double error){
        double actual = cut.getLengthBetweenCars(firstSpeed, secondSpeed, distanceBetween, time);
        Assertions.assertEquals(expected, actual, error);
    }
    @ParameterizedTest
    @MethodSource("getIsPointInFigureTestArgs")
    void findOutIfPointInFigureTest(double X, double Y, int expected){
        double actual = cut.getIsPointInFigure(X, Y);
        Assertions.assertEquals(expected, actual);
  }

  @ParameterizedTest
  @MethodSource("getResultOfBigFormulaTestArgs")
    void getResultOfBigFormulaTestArgs (double x, double expected, double error){
        double actual = cut.getResultOfBigFormula(x);
        Assertions.assertEquals(expected, actual, error);
  }

    }
package io.services;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import service.FileService;
import service.StreamService;

import java.io.*;
import static org.junit.jupiter.api.Assertions.*;

class FileServiceTest {

    private final StreamService streamService = Mockito.mock(StreamService.class);
    private final File file1 = Mockito.mock(File.class);
    private final File file2 = Mockito.mock(File.class);
    private final File file3 = Mockito.mock(File.class);
    private final ByteArrayOutputStream byteArrayOutputStream = Mockito.mock(ByteArrayOutputStream.class);
    private final FileInputStream fileInputStream = Mockito.mock(FileInputStream.class);
    private final FileOutputStream fileOutputStream = Mockito.mock(FileOutputStream.class);
    private final File[] files = {file2, file3};
    private final File[] emptyFiles = new File[0];
    private final File[] rewriteFiles = new File[128];
    private final FileService cut = new FileService(streamService);


    @Test
    void rewriteFileTest() throws IOException {
        Mockito.when(streamService.getByteArrayOutputStream()).thenReturn(byteArrayOutputStream);
        Mockito.when(streamService.getFileInputStream(file2)).thenReturn(fileInputStream);
        Mockito.when(streamService.getFileInputStream(file3)).thenReturn(fileInputStream);
        Mockito.when(streamService.getFileOutputStream(file1)).thenReturn(fileOutputStream);

        cut.rewriteFile(file1, files);

        Mockito.verify(streamService, Mockito.times(files.length)).OutPutToInput(byteArrayOutputStream, fileInputStream);
        Mockito.verify(byteArrayOutputStream, Mockito.times(1)).writeTo(fileOutputStream);
    }

    @Test
    void rewriteFileExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.rewriteFile(file1, emptyFiles));
        assertThrows(IllegalArgumentException.class, () -> cut.inputToEndOfFile(file1, rewriteFiles));
        assertThrows(IllegalArgumentException.class, () -> cut.rewriteFile(null, emptyFiles));
        assertThrows(IllegalArgumentException.class, () -> cut.rewriteFile(file1, null));
    }

    @Test
    void inputToStartOfFIleTest() throws IOException {
        Mockito.when(streamService.getByteArrayOutputStream()).thenReturn(byteArrayOutputStream);
        Mockito.when(streamService.getFileOutputStream(file1)).thenReturn(fileOutputStream);
        Mockito.when(streamService.getFileInputStream(file1)).thenReturn(fileInputStream);
        Mockito.when(streamService.getFileInputStream(file2)).thenReturn(fileInputStream);
        Mockito.when(streamService.getFileInputStream(file3)).thenReturn(fileInputStream);

        cut.inputToStartOfFIle(file1, files);

        Mockito.verify(streamService, Mockito.times(3)).OutPutToInput(byteArrayOutputStream, fileInputStream);
        Mockito.verify(byteArrayOutputStream, Mockito.times(1)).writeTo(fileOutputStream);
    }

    @Test
    void inputToStartOfFIleExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.inputToStartOfFIle(file1, emptyFiles));
        assertThrows(IllegalArgumentException.class, () -> cut.inputToEndOfFile(file1, rewriteFiles));
        assertThrows(IllegalArgumentException.class, () -> cut.inputToStartOfFIle(null, emptyFiles));
        assertThrows(IllegalArgumentException.class, () -> cut.inputToStartOfFIle(file1, null));
    }

    @Test
    void inputToEndOfFileTest() throws IOException {
        Mockito.when(streamService.getByteArrayOutputStream()).thenReturn(byteArrayOutputStream);
        Mockito.when(streamService.getFileOutputStream(file1)).thenReturn(fileOutputStream);
        Mockito.when(streamService.getFileInputStream(file1)).thenReturn(fileInputStream);
        Mockito.when(streamService.getFileInputStream(file2)).thenReturn(fileInputStream);
        Mockito.when(streamService.getFileInputStream(file3)).thenReturn(fileInputStream);

        cut.inputToEndOfFile(file1, files);

        Mockito.verify(streamService, Mockito.times(3)).OutPutToInput(byteArrayOutputStream, fileInputStream);
        Mockito.verify(byteArrayOutputStream, Mockito.times(1)).writeTo(fileOutputStream);
    }

    @Test
    void inputToEndOfFileExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.inputToEndOfFile(file1, emptyFiles));
        assertThrows(IllegalArgumentException.class, () -> cut.inputToEndOfFile(file1, rewriteFiles));
        assertThrows(IllegalArgumentException.class, () -> cut.inputToEndOfFile(null, emptyFiles));
        assertThrows(IllegalArgumentException.class, () -> cut.inputToEndOfFile(file1, null));
    }
}
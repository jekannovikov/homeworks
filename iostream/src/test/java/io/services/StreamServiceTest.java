package io.services;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import service.StreamService;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;

class StreamServiceTest {

    private final ByteArrayOutputStream byteArrayOutputStream = Mockito.mock(ByteArrayOutputStream.class);
    private final FileInputStream fileInputStream = Mockito.mock(FileInputStream.class);
    private final byte[] bm = new byte[100];
    private final StreamService cut = new StreamService(bm);

    @Test
    void OutPutToInputTest() throws IOException {
        int length = 10;
        Mockito.when(fileInputStream.read(bm)).thenReturn(length).thenReturn(0);

        cut.OutPutToInput(byteArrayOutputStream, fileInputStream);

        Mockito.verify(byteArrayOutputStream, Mockito.times(1)).write(bm, 0, length);
    }

    @Test
    void OutPutToInputExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.OutPutToInput(null, fileInputStream));
        assertThrows(IllegalArgumentException.class, () -> cut.OutPutToInput(byteArrayOutputStream, null));
        assertThrows(IllegalArgumentException.class, () -> cut.OutPutToInput(null, null));
    }
}
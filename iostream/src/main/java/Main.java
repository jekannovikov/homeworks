import service.FileService;
import service.StreamService;
import java.io.File;

public class Main {
    public static void main(String[] args) {

        File file1Main = new File("src/main/java/io/files/file1Main.txt");
        File file2 = new File("src/main/java/io/files/file2.txt");
        File file3 = new File("src/main/java/io/files/file3.txt");
        File file4 = new File("src/main/java/io/files/file4.txt");
        File file5 = new File("src/main/java/io/files/file5.txt");


        byte[] b = new byte[50];

        StreamService streamService = new StreamService(b);
        FileService fileService = new FileService(streamService);



        File[] files = {file2, file3, file4, file5};

        fileService.rewriteFile(file1Main, files);
        fileService.inputToStartOfFIle(file1Main, files);
        fileService.inputToEndOfFile(file1Main, files);

    }
}

package service;

import java.io.*;

public class StreamService {

    private byte[] buff;

    public StreamService(byte[] buff) {
        this.buff = buff;
    }

    public void OutPutToInput (ByteArrayOutputStream byteArrayOutputStream, FileInputStream fileInputStream) throws IOException {
        if (byteArrayOutputStream == null || fileInputStream == null) {
            throw new IllegalArgumentException();
        }
        int data;
        while ((data = fileInputStream.read(buff)) > 0) {
            byteArrayOutputStream.write(buff, 0, data);
        }
    }

    public ByteArrayOutputStream getByteArrayOutputStream() {

        return new ByteArrayOutputStream();
    }

    public FileInputStream getFileInputStream(File file) throws IOException {

        return new FileInputStream(file);
    }

    public FileOutputStream getFileOutputStream(File file) throws IOException {

        return new FileOutputStream(file);
    }
}

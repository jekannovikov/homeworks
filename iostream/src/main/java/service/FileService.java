package service;

import java.io.*;

public class FileService {

    private StreamService streamService;

    public FileService(StreamService streamService) {

        this.streamService = streamService;
    }

    public void rewriteFile(File file, File[] data) {
        if (file == null || data == null || data.length == 0 || data.length > 127) {
            throw new IllegalArgumentException();
        }

        try (ByteArrayOutputStream byteArrayOutputStream = streamService.getByteArrayOutputStream()) {

            for (int i = 0; i < data.length; i++) {
                if (data[i] == null) {
                    continue;
                }
                try (FileInputStream fileInputStream = streamService.getFileInputStream(data[i])) {
                    streamService.OutPutToInput(byteArrayOutputStream, fileInputStream);
                }
            }

            try (FileOutputStream fileOutputStream = streamService.getFileOutputStream(file)) {
                byteArrayOutputStream.writeTo(fileOutputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void inputToStartOfFIle(File file, File[] data) {
        if (file == null || data == null || data.length == 0 || data.length > 127) {
            throw new IllegalArgumentException();
        }
        try (ByteArrayOutputStream byteArrayOutputStream = streamService.getByteArrayOutputStream()) {

            for (int i = 0; i < data.length; i++) {
                if (data[i] == null) {
                    continue;
                }
                try (FileInputStream fileInputStream = streamService.getFileInputStream(data[i])) {
                    streamService.OutPutToInput(byteArrayOutputStream, fileInputStream);
                }
            }

            try (FileInputStream fileInputStream = streamService.getFileInputStream(file)) {
                streamService.OutPutToInput(byteArrayOutputStream, fileInputStream);
            }

            try (FileOutputStream fileOutputStream = streamService.getFileOutputStream(file)) {
                byteArrayOutputStream.writeTo(fileOutputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public void inputToEndOfFile(File file, File[] data) {
        if (file == null || data == null || data.length == 0 || data.length > 127) {
            throw new IllegalArgumentException();
        }
        try (ByteArrayOutputStream byteArrayOutputStream = streamService.getByteArrayOutputStream()) {

            try (FileInputStream fileInputStream = streamService.getFileInputStream(file)) {
                streamService.OutPutToInput(byteArrayOutputStream, fileInputStream);
            }

            for (int i = 0; i < data.length; i++) {
                if (data[i] == null) {
                    continue;
                }
                try (FileInputStream fileInputStream = streamService.getFileInputStream(data[i])) {
                    streamService.OutPutToInput(byteArrayOutputStream, fileInputStream);
                }
            }

            try (FileOutputStream fileOutputStream = streamService.getFileOutputStream(file)) {
                byteArrayOutputStream.writeTo(fileOutputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

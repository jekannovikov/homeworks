package tests.com.company;

import main.com.company.ValidateUserNumResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ValidateUserNumResponseTest {
    private static ValidateUserNumResponse cut = new ValidateUserNumResponse();
    static Arguments[] validateUserNumResponseTestArgs(){
        return new Arguments[]{
                Arguments.arguments("234", 234),
                Arguments.arguments("2341", 0),
                Arguments.arguments("500", 500),
                Arguments.arguments("1", 1),
                Arguments.arguments("asd", 0),
                Arguments.arguments(null, 0),
        };
    }

    @ParameterizedTest
    @MethodSource("validateUserNumResponseTestArgs")
    public void validateUserNumResponseTest(String numResponse, int expected){
        int actual = cut.validateUserNumResponse(numResponse);
        Assertions.assertEquals(expected, actual);
    }
}

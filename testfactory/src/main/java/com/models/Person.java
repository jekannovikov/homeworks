package com.models;

import java.io.Serializable;
import java.util.Objects;


public class Person implements Serializable {

    private int id;
    private String fname;
    private String lname;
    private int age;
    private String city;

    public Person(int id, String fname, String lname, int age, String city) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.age = age;
        this.city = city;
    }

    public Person() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Person ==> " +
                "  id: " + id +
                " <> fName: " + fname +
                " <> lName: " + lname +
                " <> age: " + age +
                " <> city: " + city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return getId() == person.getId() && getAge() == person.getAge() && Objects.equals(getFname(), person.getFname()) && Objects.equals(getLname(), person.getLname()) && Objects.equals(getCity(), person.getCity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFname(), getLname(), getAge(), getCity());
    }

}
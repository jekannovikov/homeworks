package com.gui;

import com.impl.CommandSevice;
import com.impl.Instance;
import com.models.Person;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.util.List;


public class GuiSwing extends JFrame {

    JButton createButton;
    JButton updateButton;
    JButton deleteButton;

    private static final String[] columnNames = {
            "ID",
            "First Name",
            "Last Name",
            "Age",
            "City"
    };


    DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0);
    JTable table = new JTable(tableModel);
    JScrollPane scrollPanel = new JScrollPane(table);


    public void setTable(List<Person> list) {

        for (Person person : list) {
            setRow(person);
        }
    }

    public String[] convertPersonStr(Person person) {
        String[] personStr = new String[5];
        personStr[0] = Integer.toString(person.getId());
        personStr[1] = person.getFname();
        personStr[2] = person.getLname();
        personStr[3] = Integer.toString(person.getAge());
        personStr[4] = person.getCity();
        return personStr;
    }

    public void setRow(Person person) {
        tableModel.addRow(convertPersonStr(person));
    }

    JTextField currentInstanceField;
    String env;

    public GuiSwing() {

        super("Project v2.0");

        final JLabel currentInstanceLabel = new JLabel("Environment");
        currentInstanceLabel.setBounds(1060, 0, 100, 20);

        final JLabel dataBases = new JLabel("Data Bases");
        dataBases.setBounds(1065, 265, 100, 20);

        final JLabel fileBases = new JLabel("File Bases");
        fileBases.setBounds(1065, 55, 100, 20);

        final JLabel commands = new JLabel("Commands");
        commands.setBounds(1065, 600, 100, 20);

        currentInstanceField = new JTextField("choose environment");
        currentInstanceField.setHorizontalAlignment(JTextField.CENTER);
        currentInstanceField.setBounds(1020, 20, 150, 20);

        JButton jsonButton = new JButton("JSON");   //создание новых кнопок
        JButton xmlButton = new JButton("XML");
        JButton yamlButton = new JButton("YAML");
        JButton csvButton = new JButton("CSV");
        JButton binaryButton = new JButton("BINARY");
        JButton cassandraButton = new JButton("CASSANDRA");
        JButton postgreButton = new JButton("POSTGRE");
        JButton h2Button = new JButton("H2");
        JButton mongoButton = new JButton("MONGO");
        JButton redisButton = new JButton("REDIS");
        JButton graphButton = new JButton("GRAPH");
        JButton mysqlButton = new JButton("MYSQL");

        createButton = new JButton("CREATE");
        updateButton = new JButton("UPDATE");
        deleteButton = new JButton("DELETE");

        scrollPanel.setBounds(5, 5, 1000, 750);

        jsonButton.setBounds(1020, 75, 150, 30);   //кнопки
        xmlButton.setBounds(1020, 105, 150, 30);
        yamlButton.setBounds(1020, 135, 150, 30);
        csvButton.setBounds(1020, 165, 150, 30);
        binaryButton.setBounds(1020, 195, 150, 30);
        cassandraButton.setBounds(1020, 285, 150, 30);
        postgreButton.setBounds(1020, 315, 150, 30);
        h2Button.setBounds(1020, 345, 150, 30);
        mongoButton.setBounds(1020, 375, 150, 30);
        redisButton.setBounds(1020, 405, 150, 30);
        graphButton.setBounds(1020, 435, 150, 30);
        mysqlButton.setBounds(1020, 465, 150, 30);

        createButton.setBounds(1020, 620, 150, 30);
        createButton.setVisible(false);
        updateButton.setBounds(1020, 650, 150, 30);
        updateButton.setVisible(false);
        deleteButton.setBounds(1020, 680, 150, 30);
        deleteButton.setVisible(false);


        add(jsonButton);  //добавление кнопок в главное окно
        add(xmlButton);
        add(yamlButton);
        add(csvButton);
        add(binaryButton);
        add(cassandraButton);
        add(postgreButton);
        add(h2Button);
        add(mongoButton);
        add(redisButton);
        add(graphButton);
        add(mysqlButton);

        add(currentInstanceLabel);
        add(currentInstanceField);

        add(createButton);
        add(updateButton);
        add(deleteButton);

        add(dataBases);
        add(fileBases);
        add(commands);

        add(scrollPanel);

        setSize(1200, 800);
        setLocationRelativeTo(null);
        setResizable(true);
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        ActionListener actionListener = new TestActionListener(this);


        jsonButton.addActionListener(actionListener);
        csvButton.addActionListener(actionListener);
        yamlButton.addActionListener(actionListener);
        binaryButton.addActionListener(actionListener);
        xmlButton.addActionListener(actionListener);
        mysqlButton.addActionListener(actionListener);
        postgreButton.addActionListener(actionListener);
        h2Button.addActionListener(actionListener);
        redisButton.addActionListener(actionListener);
        cassandraButton.addActionListener(actionListener);
        graphButton.addActionListener(actionListener);
        mongoButton.addActionListener(actionListener);

        createButton.addActionListener(actionListener);
        updateButton.addActionListener(actionListener);
        deleteButton.addActionListener(actionListener);

    }

    public void setVisibleCrudButtons(Instance instance) {
        if (instance != null) {
            createButton.setVisible(true);
            updateButton.setVisible(true);
            deleteButton.setVisible(true);
        }
    }

    public void setEnvironmentField(Instance instance) {
        env = instance.toString();
        currentInstanceField.setText(env);
    }

    public void createModalWindow(CommandSevice commandSevice, Instance instance) {
        boolean exception = true;

        JTextField id = new JTextField();
        JTextField firstName = new JTextField();
        JTextField lastName = new JTextField();
        JTextField age = new JTextField();
        JTextField city = new JTextField();

        final JComponent[] inputs = new JComponent[]{
                new JLabel("ID"), id,
                new JLabel("FirstName"), firstName,
                new JLabel("LastName"), lastName,
                new JLabel("Age"), age,
                new JLabel("City"), city
        };

        if (0 == JOptionPane.showConfirmDialog(null, inputs, "Create new record", JOptionPane.OK_CANCEL_OPTION)) {

            if (true) {
                try {
                    int agePerson = Integer.parseInt(age.getText());
                    int idPerson = Integer.parseInt(id.getText());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Wrong input, <ID> and <age> must be numbers!");
                    exception = false;
                    createModalWindow(commandSevice, instance);
                }
            } else if(true) {
                String checkFName = firstName.getText();
                String checkLName = lastName.getText();
                String checkCity = city.getText();

                if (checkCity.equals("") || checkLName.equals("") || checkFName.equals("")) {
                    exception = false;
                    JOptionPane.showMessageDialog(null, "All fields must be filled!");
                    createModalWindow(commandSevice, instance);
                }
            }

            if (exception) {
                String idPerson = id.getText();
                String agePerson = age.getText();
                String fnamePerson = firstName.getText();
                String lnamePerson = lastName.getText();
                String cityPerson = city.getText();

                Person person = new Person();

                person.setId(Integer.parseInt(idPerson));
                person.setAge(Integer.parseInt(agePerson));
                person.setFname(fnamePerson);
                person.setLname(lnamePerson);
                person.setCity(cityPerson);

                JOptionPane.showMessageDialog(null, person.toString());

                commandSevice.createPersonInInstance(instance, person);
            }
        }

    }

    public void updateModalWindow(CommandSevice commandSevice, Instance instance) {
        boolean exception = true;
        Person updatePerson = getPersonFromTable();

        JTextField id = new JTextField();
        JTextField firstName = new JTextField();
        JTextField lastName = new JTextField();
        JTextField age = new JTextField();
        JTextField city = new JTextField();

        id.setText(Integer.toString(updatePerson.getId()));
        firstName.setText(updatePerson.getFname());
        lastName.setText(updatePerson.getLname());
        age.setText(Integer.toString(updatePerson.getAge()));
        city.setText(updatePerson.getCity());

        final JComponent[] inputs = new JComponent[]{
                new JLabel("FirstName"), firstName,
                new JLabel("LastName"), lastName,
                new JLabel("Age"), age,
                new JLabel("City"), city
        };

        if (0 == JOptionPane.showConfirmDialog(null, inputs, "Update record", JOptionPane.OK_CANCEL_OPTION)) {

            if (true) {
                try {
                    int agePerson = Integer.parseInt(age.getText());
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Wrong input, <age> must be numbers!");
                    exception = false;
                    updateModalWindow(commandSevice, instance);
                }
            } else if (true) {
                String checkFName = firstName.getText();
                String checkLName = lastName.getText();
                String checkCity = city.getText();

                if (checkCity.equals("") || checkLName.equals("") || checkFName.equals("")) {
                    exception = false;
                    JOptionPane.showMessageDialog(null, "All fields must be filled!");
                    updateModalWindow(commandSevice, instance);
                }
            }

            if (exception) {
                String idPerson = id.getText();
                String agePerson = age.getText();
                String fnamePerson = firstName.getText();
                String lnamePerson = lastName.getText();
                String cityPerson = city.getText();

                Person personAfterUpdate = new Person();

                personAfterUpdate.setId(Integer.parseInt(idPerson));
                personAfterUpdate.setAge(Integer.parseInt(agePerson));
                personAfterUpdate.setFname(fnamePerson);
                personAfterUpdate.setLname(lnamePerson);
                personAfterUpdate.setCity(cityPerson);

                commandSevice.updatePersonInInstance(instance, personAfterUpdate);

            }
        }

    }

    public void deleteModalWindow(CommandSevice commandSevice, Instance instance) {
        Person deletePerson = getPersonFromTable();
        if (0 == JOptionPane.showConfirmDialog(null, "Are you sure to delete?" + deletePerson.toString(), "Delete record", JOptionPane.OK_CANCEL_OPTION)) {
            commandSevice.deletePersonInInstance(instance, deletePerson);
        }
    }

    public void showBuyMessage() {
        JOptionPane.showMessageDialog(null, "Buy full version for use this DATABASE!");
    }

    private Person getPersonFromTable() {

        try {
            int index = table.getSelectedRow();

            Person selectedPerson = new Person();

            String id = (String) table.getValueAt(index, 0);
            String fName = (String) table.getValueAt(index, 1);
            String lName = (String) table.getValueAt(index, 2);
            String age = (String) table.getValueAt(index, 3);
            String city = (String) table.getValueAt(index, 4);

            int idInt = Integer.parseInt(id);
            int ageInt = Integer.parseInt(age);

            selectedPerson.setId(idInt);
            selectedPerson.setAge(ageInt);
            selectedPerson.setFname(fName);
            selectedPerson.setLname(lName);
            selectedPerson.setCity(city);


            return selectedPerson;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Chose person(click the button from mouse)!");
        }
        return null;
    }
}


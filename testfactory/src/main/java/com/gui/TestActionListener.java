package com.gui;

import com.impl.CommandSevice;
import com.impl.Factory;
import com.impl.Instance;
import com.models.Person;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class TestActionListener implements ActionListener {
    Person person;
    Factory factory = new Factory();
    CommandSevice commandsevice = new CommandSevice(factory);
    private Instance instance;
    private GuiSwing guiSwing;


    public TestActionListener(GuiSwing guiSwing) {
        this.guiSwing = guiSwing;
    }


    @Override
    public void actionPerformed(ActionEvent e) {

        String buttonCRUD = e.getActionCommand();
        String buttonEnvironment = e.getActionCommand();

        System.out.println(buttonEnvironment);

       switch (buttonEnvironment) {

           case "JSON":
               instance = Instance.JSON;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "CSV":
               instance = Instance.CSV;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "XML":
               instance = Instance.XML;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "BINARY":
               instance = Instance.BINARY;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "YAML":
               instance = Instance.YAML;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "REDIS":
               instance = Instance.REDIS;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "H2":
               instance = Instance.H2;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "MONGO":
               instance = Instance.MONGO;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "MYSQL":
               instance = Instance.MYSQL;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "POSTGRE":
               instance = Instance.POSTGRE;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "GRAPH":
               instance = Instance.GRAPH;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;
           case "CASSANDRA":
               instance = Instance.CASSANDRA;
               guiSwing.setVisibleCrudButtons(instance);
               guiSwing.setEnvironmentField(instance);
               commandsevice.readPersonInInstance(instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
       }

       switch (buttonCRUD) {
           case "DELETE":
               guiSwing.deleteModalWindow(commandsevice, instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "UPDATE":
               guiSwing.updateModalWindow(commandsevice, instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           case "CREATE":
               guiSwing.createModalWindow(commandsevice, instance);
               guiSwing.tableModel.setRowCount(0);
               guiSwing.setTable(commandsevice.readPersonInInstance(instance).read());
               break;

           default:

               break;
       }

    }

}
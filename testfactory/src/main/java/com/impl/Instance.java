package com.impl;

public enum Instance {

    JSON,
    CSV,
    BINARY,
    YAML,
    REDIS,
    XML,
    H2,
    MONGO,
    MYSQL,
    POSTGRE,
    GRAPH,
    CASSANDRA


}


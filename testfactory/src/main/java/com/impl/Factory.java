package com.impl;

import com.service.databases.*;
import com.service.files.*;

import java.io.File;

public class Factory {

    public ICrud createInstance(Instance instance) {
        ICrud crud = null;



    switch (instance) {
        case CSV:
            crud = new CSV(new File("src/main/resources/Persons.csv"));
            break;
        case JSON:
            crud = new Json(new File("src/main/resources/Persons.json"));
            break;
        case YAML:
            crud = new YAML(new File("src/main/resources/Persons.yaml"));
            break;
        case XML:
            crud = new XML(new File("src/main/resources/Persons.xml"));
            break;
        case BINARY:
            crud = new BINARY(new File("src/main/resources/Persons.binary"));
            break;
        case REDIS:
            crud = new Redis();
            break;
        case H2:
            crud = new H2();
            break;
        case POSTGRE:
            crud = new Postgre();
            break;
        case MYSQL:
            crud = new MySQL();
            break;
        case MONGO:
            crud = new MongoDB();
            break;
        case GRAPH:
            crud = new Graph();
            break;
        case CASSANDRA:
            crud = new Cassandra();
            break;
        default:
            System.out.println("DEFAULT SWWITCHCASE");
            break;
    }
        return crud;
    }
}
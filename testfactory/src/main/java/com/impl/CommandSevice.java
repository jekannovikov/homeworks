package com.impl;

import com.models.Person;

public class CommandSevice {

    private final Factory factory;

    public CommandSevice(Factory factory) {
        this.factory = factory;
    }

    public ICrud readPersonInInstance(Instance instance) {
        ICrud crud = factory.createInstance(instance);
        crud.read();
        return crud;
    }

    public ICrud createPersonInInstance(Instance instance, Person person) {
        ICrud crud = factory.createInstance(instance);
        crud.create(person);
        return crud;
    }

    public ICrud updatePersonInInstance(Instance instance, Person person) {
        ICrud crud = factory.createInstance(instance);
        crud.update(person);
        return crud;
    }

    public ICrud deletePersonInInstance(Instance instance, Person person) {
        ICrud crud = factory.createInstance(instance);
        crud.delete(person);
        return crud;
    }

}


package com.impl;

import com.models.Person;

import java.util.List;

public interface ICrud {

    List<Person> read();

    void create (Person person);

    void delete (Person person);

    void update(Person person);

}

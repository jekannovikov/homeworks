package com.service.databases;

import com.impl.ICrud;
import com.models.Person;
import com.google.gson.Gson;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Redis implements ICrud {

    private static final String REDIS_HOST = "localhost";
//        private static final String REDIS_HOST = "34.125.202.208";
    private static final int REDIS_PORT = 6379;

    Gson gson = new Gson();

    @Override
    public List<Person> read() {

        Jedis jedis = new Jedis(REDIS_HOST, REDIS_PORT);
        List<Person> personList = new ArrayList<>();

        Set keys = jedis.keys("*");
        Iterator iterator = keys.iterator();
        while (iterator.hasNext()) {
            String key = (String) iterator.next();
            String jsonPerson = jedis.get(key);
            Person person = gson.fromJson(jsonPerson, Person.class);
            personList.add(person);
        }
        jedis.close();
        return personList;

    }

    @Override
    public void create(Person person) {

        Jedis jedis = new Jedis(REDIS_HOST, REDIS_PORT);

        String createPerson = gson.toJson(person);
        String key = Integer.toString(person.getId());

        jedis.append(key, createPerson);
        jedis.close();

    }

    @Override
    public void delete(Person person) {

        Jedis jedis = new Jedis(REDIS_HOST, REDIS_PORT);

        String key = Integer.toString(person.getId());
        jedis.del(key);
        jedis.close();

    }

    @Override
    public void update(Person person) {

        Jedis jedis = new Jedis(REDIS_HOST, REDIS_PORT);

        String key = Integer.toString(person.getId());
        jedis.del(key);
        String updatePerson = gson.toJson(person);
        jedis.append(key, updatePerson);
        jedis.close();

    }
}

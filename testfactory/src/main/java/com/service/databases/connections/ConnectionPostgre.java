package com.service.databases.connections;


import com.service.databases.exceptions.FailedConnectionException;

import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionPostgre {

    private static final String DB_URL = "jdbc:postgresql://34.125.202.208/base4project";
    private static final String USER = "admin4";
    private static final String PASSWORD = "qwerty123";

    public static java.sql.Connection getConnectionToPostgre() throws FailedConnectionException {
        try {
            Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            throw new FailedConnectionException(e.getMessage());
        }
    }
}
package com.service.databases.connections;



import com.service.databases.exceptions.FailedConnectionException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionH2 {

    private static final String DB_URL = "jdbc:h2:tcp://localhost/~/test";
    private static final String USER = "denis";
    private static final String PASSWORD = "qwe123";

    public static Connection getConnectionH2() throws FailedConnectionException {
        try {
             Class.forName("org.h2.Driver");
            return DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            throw new FailedConnectionException(e.getMessage());
        }
    }
}

package com.service.databases.connections;


import com.service.databases.exceptions.FailedConnectionException;

import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionMySQL {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/javabase?useUnicode=true&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASSWORD = "1234qwer";

//    private static final String DB_URL = "jdbc:mysql://34.125.202.208:3306/javabase4?useUnicode=true&serverTimezone=UTC";
//    private static final String USER = "root";
//    private static final String PASSWORD = "qwerty123";

    public static java.sql.Connection getConnectionToMySQL() throws FailedConnectionException {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection(DB_URL, USER, PASSWORD);
        } catch (ClassNotFoundException | SQLException e) {
            throw new FailedConnectionException(e.getMessage());
        }
    }
}

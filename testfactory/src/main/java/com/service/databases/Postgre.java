package com.service.databases;


import com.impl.ICrud;
import com.models.Person;
import com.service.databases.connections.ConnectionPostgre;
import com.service.databases.exceptions.FailedConnectionException;

import java.sql.*;
import java.util.ArrayList;

public class Postgre implements ICrud {

    private static final String TABLE = "CREATE TABLE IF NOT EXISTS people (" +
            "id int8 NOT NULL PRIMARY KEY, " +
            "fname CHARACTER VARYING(1500), " +
            "lname CHARACTER VARYING(1500) NOT NULL, " +
            "age int8 NOT NULL, city CHARACTER VARYING(168) NOT NULL," +
            "CONSTRAINT people_age_check CHECK(age >0 AND age < 120)" +
            ");";
    private static final String CREATE_SCRIPT = "INSERT INTO people VALUES (?, ?, ?, ?, ?);";
    private static final String DELETE_SCRIPT = "DELETE FROM people WHERE id = ?;";
    private static final String UPDATE_SCRIPT = "UPDATE people SET fname = ?, lname = ?, age = ?, city = ? WHERE id = ?;";
    private static final String READ_SCRIPT = "SELECT * FROM people;";

    public Postgre() {
        createTable();
    }

    @Override
    public void create(Person person) {
        try (
                Connection connection = ConnectionPostgre.getConnectionToPostgre();
                PreparedStatement ps = connection.prepareStatement(CREATE_SCRIPT);
        ) {
            ps.setInt(1,person.getId());
            ps.setString(2, person.getFname());
            ps.setString(3, person.getLname());
            ps.setInt(4, person.getAge());
            ps.setString(5, person.getCity());
            ps.execute();
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Person> read() {
        ArrayList<Person> people = new ArrayList<>();
        try (
                Connection connection = ConnectionPostgre.getConnectionToPostgre();
                PreparedStatement ps = connection.prepareStatement(READ_SCRIPT);
        ) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                Person newPerson = new Person();

                int id = resultSet.getInt("id");
                String fname = resultSet.getString("fname");
                String lname = resultSet.getString("lname");
                int age = resultSet.getInt("age");
                String city = resultSet.getString("city");

                newPerson.setId(id);
                newPerson.setFname(fname);
                newPerson.setLname(lname);
                newPerson.setAge(age);
                newPerson.setCity(city);

                people.add(newPerson);
            }
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
        return people;
    }

    @Override
    public void update(Person person) {
        try (
                Connection connection = ConnectionPostgre.getConnectionToPostgre();
                PreparedStatement ps = connection.prepareStatement(UPDATE_SCRIPT)
        ) {
            ps.setString(1, person.getFname());
            ps.setString(2, person.getLname());
            ps.setInt(3, person.getAge());
            ps.setString(4, person.getCity());
            ps.setInt(5, person.getId());
            ps.execute();
        } catch (FailedConnectionException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Person person) {
        try (
                Connection connection = ConnectionPostgre.getConnectionToPostgre();
                PreparedStatement ps = connection.prepareStatement(DELETE_SCRIPT);
        ) {
            ps.setInt(1, person.getId());
            ps.execute();
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }

    private void createTable() {
        try (
                Connection connection = ConnectionPostgre.getConnectionToPostgre();
                Statement statement = connection.createStatement();
        ) {
            statement.executeUpdate(TABLE);
        } catch (SQLException | FailedConnectionException e) {
            e.printStackTrace();
        }
    }
}

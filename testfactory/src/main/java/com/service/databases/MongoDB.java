package com.service.databases;

import com.impl.ICrud;
import com.models.Person;
import com.mongodb.client.*;
import com.mongodb.client.model.Updates;

import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Filters.eq;

public class MongoDB implements ICrud {

    private static final String DB_URL = "mongodb://127.0.0.1:27017";
    private static final String DB_NAME = "mongoBase";
    private static final String COLLECTION_NAME = "personsM";

    @Override
    public List<Person> read() {

        List<Person> bufferPerson = new ArrayList<>();

        try (MongoClient mongoClient = MongoClients.create(DB_URL)) {
            MongoDatabase database = mongoClient.getDatabase(DB_NAME);
            MongoCollection<Document> personsMongoCollection = database.getCollection(COLLECTION_NAME);

            FindIterable<Document> iterDoc = personsMongoCollection.find();

            for (Document readDocument : iterDoc) {
                Person readPerson = new Person();
                readPerson.setId(Integer.parseInt(String.valueOf(readDocument.get("id"))));
                readPerson.setFname(readDocument.getString("fname"));
                readPerson.setLname(readDocument.getString("lname"));
                readPerson.setAge(Integer.parseInt(String.valueOf(readDocument.get("age"))));
                readPerson.setCity(readDocument.getString("city"));
                bufferPerson.add(readPerson);
            }
            return bufferPerson;
        }

    }

    @Override
    public void create(Person person) {

        try( MongoClient mongoClient = MongoClients.create(DB_URL)){
            MongoDatabase database = mongoClient.getDatabase(DB_NAME);
            MongoCollection<Document> personsMongoCollection = database.getCollection(COLLECTION_NAME);
            Document document = new Document(Map.of("id", person.getId(), "fname", person.getFname(), "lname",
                    person.getLname(), "age", person.getAge(), "city", person.getCity()));
            personsMongoCollection.insertOne(document);
        }

    }

    @Override
    public void delete(Person person) {

        try( MongoClient mongoClient = MongoClients.create(DB_URL)){
            MongoDatabase database = mongoClient.getDatabase(DB_NAME);
            MongoCollection<Document> personsMongoCollection = database.getCollection(COLLECTION_NAME);
            personsMongoCollection.deleteOne(eq("id", person.getId()));
        }

    }

    @Override
    public void update(Person person) {

        try( MongoClient mongoClient = MongoClients.create(DB_URL)){
            MongoDatabase database = mongoClient.getDatabase(DB_NAME);
            MongoCollection<Document> personsMongoCollection = database.getCollection(COLLECTION_NAME);
            personsMongoCollection.updateOne(eq("id", person.getId()), Updates.set("fname", person.getFname()));
            personsMongoCollection.updateOne(eq("id", person.getId()), Updates.set("lname", person.getLname()));
            personsMongoCollection.updateOne(eq("id", person.getId()), Updates.set("age", person.getAge()));
            personsMongoCollection.updateOne(eq("id", person.getId()), Updates.set("city", person.getCity()));
        }

    }

}
package com.service.files;

import com.impl.ICrud;
import com.models.Person;
import com.opencsv.CSVWriter;
import com.opencsv.bean.*;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class CSV implements ICrud {

    private File file;

    public CSV(File file) {
        this.file = file;
    }

    @Override
    public List<Person> read() {
        return readCSV();
    }

    @Override
    public void create(Person person) {
        if (person == null) {
            return;
        }
        ArrayList<Person> buff;
        List<Person> l = readCSV();
        if (l == null) {
            buff = new ArrayList<>();
        } else {
            buff = new ArrayList<>(l);
        }
        buff.add(person);
        writeCSV(buff);
    }

    @Override
    public void delete(Person person) {
        if (person == null) {
            return;
        }
        ArrayList<Person> buff;
        List<Person> l = readCSV();
        if (l == null) {
            buff = new ArrayList<>();
        } else {
            buff = new ArrayList<>(l);
        }
        for (int i = 0; i < buff.size(); i++) {
            if (buff.get(i).getId() == person.getId()) {
                buff.remove(i);
                break;
            }
        }
        writeCSV(buff);
    }

    @Override
    public void update(Person person) {
        if (person == null) {
            return;
        }
        ArrayList<Person> buff;
        List<Person> l = readCSV();
        if (l == null) {
            buff = new ArrayList<>();
        } else {
            buff = new ArrayList<>(l);
        }
        for (int i = 0; i < buff.size(); i++) {
            if (buff.get(i).getId() == person.getId()) {
                buff.set(i, person);
                break;
            }
        }
        writeCSV(buff);
    }

    private void writeCSV(List<Person> persons) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {

            StatefulBeanToCsv<Person> beanToCsv = new StatefulBeanToCsvBuilder<Person>(writer)
                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                    .build();

            beanToCsv.write(persons);
        } catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException |
                IOException e) {
            System.out.println("ERROR TO WRITE com.service.files.CSV ");
            e.printStackTrace();
        }
    }

    private List<Person> readCSV() {
        if (file.length() == 0) {
            return null;
        }
        List<Person> readedList = null;

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {

            HeaderColumnNameMappingStrategy<Person> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(Person.class);

            CsvToBean<Person> csvToBean = new CsvToBeanBuilder<Person>(br)
                    .withMappingStrategy(strategy)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            readedList = csvToBean.parse();
        } catch (IOException e) {
            System.out.println("ERROR TO READ CSV ");
            e.printStackTrace();
        }
        return readedList;
    }
}

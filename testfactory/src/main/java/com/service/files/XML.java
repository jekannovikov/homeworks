package com.service.files;

import com.impl.ICrud;
import com.models.Person;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XML implements ICrud {

    private File file;

    private XmlMapper mapper = new XmlMapper();


    public XML(File file) {
        this.file = file;
    }


    @Override
    public List<Person> read() {
        List<Person> people = readXML();
        return people;
    }

    @Override
    public void create(Person person) {
        List<Person> people = readXML();
        people.add(person);
        writeXML(people);
    }

    @Override
    public void delete(Person person) {
        List<Person> people = readXML();
        people.remove(person);
        writeXML(people);
    }

    @Override
    public void update(Person person) {
        List<Person> people = readXML();
        for (Person next : people) {
            if (next.getId() == person.getId()) {
                people.remove(next);
                people.add(person);
                break;
            }
        }
        writeXML(people);
    }

    private List<Person> readXML() {
        if (file.length() == 0) {
            return new ArrayList<Person>();
        }
        ArrayList<Person> newPeople = null;
        Person[] persons = null;
        try {
            persons = mapper.readValue(file, Person[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        newPeople = new ArrayList<Person>(List.of(persons));
        return newPeople;
    }

    private void writeXML(List<Person> persons) {
        try {
            mapper.writeValue(file, persons);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

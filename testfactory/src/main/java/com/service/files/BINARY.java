package com.service.files;

import com.impl.ICrud;
import com.models.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BINARY implements ICrud {

    private File file;

    public void setFile(File file) {
        this.file = file;
    }

    public BINARY(File file) {
        setFile(file);
    }


    @Override
    public List<Person> read() {
        List<Person> people = readBINARY();
        return people;
    }

    @Override
    public void create(Person person) {
        List<Person> people = readBINARY();
        people.add(person);
        writeBINARY(people);
    }

    @Override
    public void delete(Person person) {
        List<Person> people = readBINARY();
        people.remove(person);
        writeBINARY(people);
    }

    @Override
    public void update(Person person) {
        List<Person> people = readBINARY();
        for (Person next : people) {
            if (next.getId() == person.getId()) {
                people.remove(next);
                people.add(person);
                break;
            }
        }
        writeBINARY(people);
    }


        public List<Person> readBINARY() {
            if (file.length() == 0) {
                return new ArrayList<Person>();
            } else {
                List<Person> newPeople = new ArrayList<>();
                try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                    newPeople = ((List<Person>) ois.readObject());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return newPeople;
            }
        }

        public void writeBINARY(List<Person> personArrayList) {
            try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
                oos.writeObject(personArrayList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
}

package com.service.files;

import com.impl.ICrud;
import com.models.Person;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Json implements ICrud {

    private File file;
    private ObjectMapper mapper = new ObjectMapper();


    public Json(File file) {
        this.file = file;
    }


     @Override
    public List<Person> read() {
         List<Person> people = readJson();
         return people;
    }

    @Override
    public void create(Person person) {
        List<Person> people = readJson();
        people.add(person);
        writeJson(people);
    }

    @Override
    public void delete(Person person) {
        List<Person> people = readJson();
        people.remove(person);
        writeJson(people);
    }

    @Override
    public void update(Person person) {
        List<Person> people = readJson();
        for (Person next : people) {
            if (next.getId() == person.getId()) {
                people.remove(next);
                people.add(person);
                break;
            }
        }
        writeJson(people);
    }

    private List<Person> readJson() {
        if (file.length() == 0) {
            return new ArrayList<Person>();
        }
        ArrayList<Person> newPeople = null;
        Person[] persons = null;
        try {
            persons = mapper.readValue(file, Person[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        newPeople = new ArrayList<Person>(List.of(persons));
        return newPeople;
    }

    private void writeJson(List<Person> persons) {
        try {
            mapper.writeValue(file, persons);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

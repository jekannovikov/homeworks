package com.service.files;

import com.impl.ICrud;
import com.models.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class YAML implements ICrud {

    private File file;
    private ObjectMapper mapper = new ObjectMapper(new YAMLFactory());


    public YAML(File file) {
        this.file = file;
    }


    @Override
    public List<Person> read() {
        List<Person> people = readYAML();
        return people;
    }

    @Override
    public void create(Person person) {
        List<Person> people = readYAML();
        people.add(person);
        writeYAML(people);
    }

    @Override
    public void delete(Person person) {
        List<Person> people = readYAML();
        people.remove(person);
        writeYAML(people);
    }

    @Override
    public void update(Person person) {
        List<Person> people = readYAML();
        for (Person next : people) {
            if (next.getId() == person.getId()) {
                people.remove(next);
                people.add(person);
                break;
            }
        }
        writeYAML(people);
    }

    private List<Person> readYAML() {
        if (file.length() == 0) {
            return new ArrayList<Person>();
        }
        ArrayList<Person> newPeople = null;
        Person[] persons = null;
        try {
            persons = mapper.readValue(file, Person[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        newPeople = new ArrayList<Person>(List.of(persons));
        return newPeople;
    }

    private void writeYAML(List<Person> persons) {
        try {
            mapper.writeValue(file, persons);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

package com.java.services;

public class PrintMatrix {

    public static void printMatrix(int[][] a) {
        for(int[] l : a) {
            for(int i : l)
                System.out.print("\t"+i);
            System.out.println();
        }
    }
}

package com.java.services;

public class CreateMatrix {




        public static int[][] createMatrixTask1(int size) {
            int[][] a = new int [size][size];
            for (int i=0; i<1; i++) {
                for (int j=0; j<size; j++) {
                    a[i][j] = 1;}}
            for (int i=6; i<7; i++){
                for (int j=0; j<size; j++){
                    a[i][j] = 1;}}
            for (int i=1; i<6; i++){
                for (int j=0; j<1; j++){
                    a[i][j] = 1;}}
            for (int i=1; i<6; i++){
                for (int j=6; j<7; j++){
                    a[i][j] = 1;}}



            return a;
        }


        public static int[][] createMatrixTask2(int size) {
            int[][] a = new int [size][size];
            for (int i=0; i<1; i++) {
                for (int j=0; j<size; j++) {
                    a[i][j] = 1;}}


            for (int i=1; i<6; i++){
                for (int j=0; j<1; j++){
                    a[i][j] = 1;}}
            for (int i=0; i<size; i++){
                for (int j=0; j<size; j++){
                    if (i+j == size-1){
                        a[i][j] = 1;}}}




            return a;
        }

        public static int[][] createMatrixTask3(int size) {
            int[][] a = new int [size][size];

            for (int i=6; i<7; i++){
                for (int j=0; j<size; j++){
                    a[i][j] = 1;}}
            for (int i=1; i<6; i++){
                for (int j=0; j<1; j++){
                    a[i][j] = 1;}}
            for (int i=0; i<size; i++){
                for (int j=0; j<size; j++){
                    if (i==j){
                        a[i][j] = 1;}}}




            return a;
        }


        public static int[][] createMatrixTask4(int size) {
            int[][] a = new int [size][size];

            for (int i=6; i<7; i++){
                for (int j=0; j<size; j++){
                    a[i][j] = 1;}}

            for (int i=1; i<6; i++){
                for (int j=6; j<7; j++){
                    a[i][j] = 1;}}
            for (int i=0; i<size; i++){
                for (int j=0; j<size; j++){
                    if (i+j == size-1){
                        a[i][j] = 1;}}}





            return a;
        }

        public static int[][] createMatrixTask5(int size) {
            int[][] a = new int [size][size];
            for (int i=0; i<1; i++) {
                for (int j=0; j<size; j++) {
                    a[i][j] = 1;}}


            for (int i=1; i<6; i++){
                for (int j=6; j<7; j++){
                    a[i][j] = 1;}}

            for (int i=0; i<size; i++){
                for (int j=0; j<size; j++){
                    if (i==j){
                        a[i][j] = 1;}}}




            return a;
        }

        public static int[][] createMatrixTask6(int size) {
            int[][] a = new int [size][size];
            for (int i=0; i<size; i++){
                for (int j=0; j<size; j++){
                    if (i==j){
                        a[i][j] = 1;}
                    if (i+j == size-1){
                        a[i][j] = 1;

                    }}}

            return a;
        }

        public static int[][] createMatrixTask7(int size) {
            int[][] a = new int [size][size];
            for (int i=0; i<1; i++) {
                for (int j=0; j<size; j++) {
                    a[i][j] = 1;}}
            for (int i=0; i<size; i++){
                for (int j=0; j<size; j++){
                    if (i==j && i<4){
                        a[i][j] = 1;}
                    if (i+j == size-1 && i<4){
                        a[i][j] = 1;

                    }}}

            return a;
        }

        public static int[][] createMatrixTask8(int size) {
            int[][] a = new int [size][size];

            for (int i=6; i<7; i++){
                for (int j=0; j<size; j++){
                    a[i][j] = 1;}}


            for (int i=0; i<size; i++){
                for (int j=0; j<size; j++){
                    if (i==j && i>2){
                        a[i][j] = 1;}
                    if (i+j == size-1 && i>2){
                        a[i][j] = 1;

                    }}}

            return a;
        }

    }


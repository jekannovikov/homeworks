import com.java.services.PrintMatrix;
import com.java.services.CreateMatrix;

public class Main {

    public static void main(String[] args){
        int[][] m1 = CreateMatrix.createMatrixTask1(7);
        PrintMatrix.printMatrix(m1);
        System.out.println();

        int[][] m2 = CreateMatrix.createMatrixTask2(7);
        PrintMatrix.printMatrix(m2);
        System.out.println();

        int[][] m3 = CreateMatrix.createMatrixTask3(7);
        PrintMatrix.printMatrix(m3);
        System.out.println();

        int[][] m4 = CreateMatrix.createMatrixTask4(7);
        PrintMatrix.printMatrix(m4);
        System.out.println();

        int[][] m5 = CreateMatrix.createMatrixTask5(7);
        PrintMatrix.printMatrix(m5);
        System.out.println();

        int[][] m6 = CreateMatrix.createMatrixTask6(7);
        PrintMatrix.printMatrix(m6);
        System.out.println();

        int[][] m7 = CreateMatrix.createMatrixTask7(7);
        PrintMatrix.printMatrix(m7);
        System.out.println();

        int[][] m8 = CreateMatrix.createMatrixTask8(7);
        PrintMatrix.printMatrix(m8);
    }
}

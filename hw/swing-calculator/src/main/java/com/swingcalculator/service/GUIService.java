package main.java.com.swingcalculator.service;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUIService extends JFrame {

    public JButton calculate = new JButton("Calculate");
    public JTextField firstNumber = new JTextField("", 10);
    public JTextField secondNumber = new JTextField("", 10);
    public JTextField operation = new JTextField("", 10);
    public JTextField result = new JTextField("", 10);
    public JLabel labelOne = new JLabel("Number 1:");
    public JLabel labelTwo = new JLabel("Number 2:");
    public JLabel operationLabel = new JLabel("Operation:");
    public JLabel resultLabel = new JLabel("Result:");

    public GUIService(){
        setTitle("Calculator");
        Font font = new Font("SansSerif", Font.BOLD, 18);
        setSize(600, 590);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = getContentPane();
        container.setLayout(null);
        container.setBackground(new Color(222, 227, 206));

        labelOne.setBounds(60, 55, 120, 40);
        labelOne.setFont(font);
        container.add(labelOne);
        firstNumber.setBounds(185, 55, 340, 40);
        container.add(firstNumber);
        labelTwo.setBounds(60, 140, 120, 40);
        labelTwo.setFont(font);
        container.add(labelTwo);
        secondNumber.setBounds(185, 140, 340, 40);
        container.add(secondNumber);
        operationLabel.setBounds(60, 225, 120, 40);
        operationLabel.setFont(font);
        container.add(operationLabel);
        operation.setBounds(185, 225, 340, 40);
        container.add(operation);
        calculate.setBounds(75, 310, 450, 60);
        calculate.addActionListener(new Calculator());
        container.add(calculate);
        resultLabel.setBounds(60, 395, 120, 40);
        resultLabel.setFont(font);
        container.add(resultLabel);
        result.setBounds(185, 395, 340, 40);
        container.add(result);

        setContentPane(container);
    }

    class Calculator implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            double numberOne = Double.parseDouble(firstNumber.getText()), numberTwo = Double.parseDouble(secondNumber.getText());
            String symbol = operation.getText(), res = "";

            if (symbol.equals("+")) {
                if((numberOne + numberTwo)-Math.floor(numberOne + numberTwo) != 0) {
                    res += numberOne + numberTwo;
                } else {
                    res += (int)(numberOne + numberTwo);
                }
                result.setText(res);
            } else
                if(symbol.equals("-")){
                    if((numberOne - numberTwo)-Math.floor(numberOne - numberTwo) != 0) {
                        res += numberOne - numberTwo;
                    } else {
                        res += (int)(numberOne - numberTwo);
                    }
                    result.setText(res);
                } else
                if(symbol.equals("*")){
                    if((numberOne * numberTwo)-Math.floor(numberOne * numberTwo) != 0) {
                        res += numberOne * numberTwo;
                    } else {
                        res += (int)(numberOne * numberTwo);
                    }
                    result.setText(res);
                } else
                if(symbol.equals("/")){
                    if((numberOne / numberTwo)-Math.floor(numberOne / numberTwo) != 0) {
                        res += numberOne / numberTwo;
                    } else {
                        res += (int)(numberOne / numberTwo);
                    }
                    result.setText(res);
                }
        }
    }
}

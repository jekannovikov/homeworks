package com.logger.services;

import com.logger.exceptions.MyCustomException;
import java.util.Random;

public class RandomService {

    private Random random;

    public RandomService(Random random) {
        this.random = random;
    }

    public int getRandomNumber() throws MyCustomException {

        int randomNumber = random.nextInt(10 - 0 + 1);
        if (randomNumber <= 5) {
            throw new MyCustomException(String.valueOf(randomNumber));
        }
        return randomNumber;
    }
}

package com.logger.exceptions;

public class MyCustomException extends Exception {

    public MyCustomException(String message) {
        super("Generated number – " + message);
    }

}

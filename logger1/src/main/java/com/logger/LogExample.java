package com.logger;

import com.logger.exceptions.MyCustomException;
import com.logger.services.RandomService;
import org.apache.log4j.Logger;
import java.util.Random;

public class LogExample {

    private static final String SUCCESSFUL = "App successful started";
    private final Logger logConsole;
    private final Logger logFile;

    public LogExample(Logger logConsole, Logger logFile) {
        this.logConsole = logConsole;
        this.logFile = logFile;
    }

    public static void main(String[] args) {

        LogExample logExample = new LogExample(Logger.getLogger("loggerConsole"), Logger.getLogger("loggerFile"));
        logExample.generate(new RandomService(new Random()), 30);
    }

    public void generate(RandomService randomService, int count) {
        for (int i = 0; i < count; i++) {
            try {
                randomService.getRandomNumber();
                logConsole.info(SUCCESSFUL);
                logFile.info(SUCCESSFUL);
            } catch (MyCustomException e) {
                logConsole.info(e.getMessage());
                logFile.info(e.getMessage());
            }
        }
    }
}

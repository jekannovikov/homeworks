package com.company.services;

import java.util.Scanner;

public class Cycles {

    public static int[] getCountAndSumOddsTask1() {

        int count = 0;
        int result = 0;
        int[] arr = {count, result};
        for (int i = 1; i <= 99; i++) {
            if ((i % 2) == 0) {
                count += 1;
                result = result + i;
            }
        }
        return arr;

    }

    public static String getCountAndSumOddsTask2(int number) {
        String answer = "";
        if (number == 1) {
            answer = "Your number are not simple";
        } else if (number == 2 || number == 3 || number == 5 || number == 7) {
            answer = "Your number are  simple";

        } else {
            for (int i = 2; i <= 9; i++) {

                if ((number % i) == 0) {
                    answer = "Your number are not simple";

                    break;
                } else if (i == 9) {
                    answer = "Your number are  simple";


                }

            }
        }
        return answer;

    }

    public static int getSqrtTask3 (int UserNumber) {

        int SqrtFloat = 1;

        while (SqrtFloat * SqrtFloat != UserNumber) {
            SqrtFloat += 1;
            if (SqrtFloat * SqrtFloat > UserNumber) {
                SqrtFloat = 1;
                UserNumber -= 1;

            }
        }
        return SqrtFloat;
    }

    public static int getSqrtByBinarySearchTask3(int UserNumber) {
        int diaposone;
        int Sqrt = 0;
        diaposone = UserNumber;

        while (diaposone * diaposone >= UserNumber) {
            diaposone /= 2;

            if (diaposone * diaposone < UserNumber) {

                for (Sqrt = diaposone; Sqrt <= diaposone * 2; Sqrt++) {
                    if (Sqrt * Sqrt == UserNumber) {


                        break;
                    }
                    if (Sqrt * Sqrt > UserNumber) {

                        Sqrt -= 1;

                        break;


                    }


                }
            }
        }
        return Sqrt;
    }

    public static int getFactorialTask4 (int number) {

    int result = 1;

     for(int i = 2;i<=number;i++){

        result = result*i;

    }
    return result;
}
    public static int getSumOfNumberTask5 (int number) {
        int sum = 0;

        while (number != 0) {
            sum = sum + (number % 10);
            number /= 10;

        }
        return sum;
    }
   public static void getReverseNumber (int number) {

       while (number > 0) {
           System.out.print(number % 10);
           number /= 10;

       }

   }

}
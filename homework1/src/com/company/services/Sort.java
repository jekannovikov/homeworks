package com.company.services;

import java.util.Arrays;

public class Sort {

    static void bubbleSort(int[] arrayUnsorted) {

        int counter = 0;

        for (int i = 0; i < arrayUnsorted.length - 1; i++)
            if (arrayUnsorted[i] > arrayUnsorted[i + 1]) {
                int temp = arrayUnsorted[i];
                arrayUnsorted[i] = arrayUnsorted[i + 1];
                arrayUnsorted[i + 1] = temp;
                counter++;
            }
        if (counter > 0) {
            bubbleSort(arrayUnsorted);
        }
    }
    public static void selectionSort(int array[]){
        for(int i = 0; i < array.length - 1; i++)
        {
            int index = i;

            for(int j = i+1; j < array.length; j++)
            {
                if(array[j] < array[index])
                {
                    index = j;
                }
            }

            int smallerNum = array[index];
            array[index] = array[i];
            array[i] = smallerNum;
        }
    }




    public static void main(String[] args) {
        int nums[] = {10, 6, 4, 5, 2, 8, 3, 1, 9, 7};
        System.out.print("Array before sort bubble: ");
        for(int a : nums){
            System.out.print(a + " ");
        }
        bubbleSort(nums);
        System.out.print("\nArray after sort bubble: ");
        for (int b : nums){
            System.out.print(b + " ");
        }
        System.out.println();
        int numsArray[] = {14, 45, 32, 17, 1, 3};

        System.out.println("Array before selection sort: " + java.util.Arrays.toString(numsArray));

        selectionSort(numsArray);

        System.out.println("Array after selection sort: " + Arrays.toString(numsArray));
    }


}

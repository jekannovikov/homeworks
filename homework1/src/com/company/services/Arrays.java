package com.company.services;

public class Arrays {


  public static int getMinValueFromArray (int SomeArr[]) {

    int[] arr = {12, 17, 236, 754, 234, 24, 4};
    int min = 999999999;

     for (int i=0;i<arr.length;i++){

        if (arr[i]<min){
            min = arr[i];
        }
    }

      return min;
  }
    public static int getMaxValueFromArray (int SomeArr[]) {

    int arr[] = {12,17,236,754,234,24,4};
    int max = 0;
     for (int i=0;i<arr.length;i++){

        if (arr[i]>max){
            max=arr[i];
        }

    }
    return max;

}
    public static int getIndexOfMinFromArray (int SomeArr[]) {
    int arr[] = {12,5,236,754,234,24,4};
    int min = 9999999;
    int tmp = 0;

     for (int i=0;i<arr.length;i++){

        if (arr[i]<min){
            min=arr[i];
            tmp=i;
        }
    }
     return tmp;
}
    public static int getIndexOfMxFromArray (int SomeArr[]) {
        int arr[] = {12,5,236,754,234,24,4};
        int max = 0;
        int tmp = 0;

        for (int i=0;i<arr.length;i++){

            if (arr[i]>max){
                max=arr[i];
                tmp=i;
            }
        }
        return tmp;
    }
    public static int getSumOfArrayNumbers (int SomeArr[]) {
    int arr[] = {12,5,236,754,234,24,4};
    int sum = 0;

     for (int i=1;i<arr.length;i+=2){

        sum = sum + arr[i];

    }
     return sum;

}

    public static int[] getReversedArray (int[] SomeArr) {

        int[] arr = {5, 46, 1, 42, 6, 4, 7, 8};
        int[] reversedArr = new int[arr.length];


        for (int i = 0; i < arr.length; i++) {
            reversedArr[i] = arr[arr.length - 1 - i];

        }
        return reversedArr;
    }

    public static int getSumOfOddArray (int[] SomeArr) {
    int[] arr = {5,46,1,42,6,4,7,8};

    int counter=0;

    for (int i=0; i<arr.length; i++) {
        if(arr[i]%2!=0){
            counter+=1;
        }
    }
    return counter;
}

    public static int[] getHardReversedArray (int[] SomeArr) {
    int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7};

    int mod = arr.length % 2;

        for (int i = 0; i < arr.length / 2; i++) {
        int temp = arr[i];

        arr[i] = arr[arr.length / 2 + i + mod];
        arr[arr.length / 2 + i + mod] = temp;
    }

        return arr;
}




}

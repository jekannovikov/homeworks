package services;

public class SortingService {

    public int[] quickSorting(int[] array, int lowInd, int highInd) {
        if (array == null || highInd > array.length - 1 || lowInd < 0) {
            return null;
        }
        //завершить выполнение, если длина массива равна 0
        if (array.length == 0) {
            return array;
        }

        //завершить выполнение если масив уже слижком маленький
        if (lowInd >= highInd) {
            return array;
        }

        // выбрать опорный элемент
        int mid = array[lowInd + (highInd - lowInd) / 2];

        // разделить на подмассивы, который больше и меньше опорного элемента
        int i = lowInd;
        int j = highInd;
        while (i <= j) {
            //ищем индекс элемента большего чем опорный в левом масиве
            while (array[i] < mid) {
                i++;
            }

            //ищем индекс элемента меньшего чем опорный в правом масиве
            while (array[j] > mid) {
                j--;
            }

            //меняем местами
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }

        // вызов рекурсии для сортировки левой и правой части
        if (lowInd < j) {
            quickSorting(array, lowInd, j);
        }

        if (highInd > i) {
            quickSorting(array, i, highInd);
        }

        return array;
    }
}

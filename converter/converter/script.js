
function ConvertSecToOther() {

let seconds = Number( document.getElementById("seconds").value );


if (seconds > 0 ) {

   document.getElementById("minute").value = Math.floor(seconds/60*100)/100;
   document.getElementById("hour").value = Math.floor(seconds/3600*100)/100;
   document.getElementById("day").value = Math.floor((seconds/3600)/24*100)/100;
   document.getElementById("week").value = Math.floor(((seconds/3600)/24)/7*100)/100;
   document.getElementById("month").value = Math.floor((((seconds/3600)/24)/7)/4.34534*100)/100;
   document.getElementById("year").value = Math.floor(((((seconds/3600)/24)/7)/4.34534)/12 *100)/100;
}


}

function OtherToSec() {
let minute = Number( document.getElementById("minute").value );
let hour = Number( document.getElementById("hour").value );
let day = Number( document.getElementById("day").value );
let week = Number( document.getElementById("week").value );
let month = Number( document.getElementById("month").value );
let year = Number( document.getElementById("year").value );
if (minute !== 0) {

   document.getElementById("seconds").value = Math.floor(minute*60);
}
if (hour !== 0) {

   document.getElementById("seconds").value = Math.floor(hour*60*60);
}
if (day !== 0) {

   document.getElementById("seconds").value = Math.floor(day*60*60*24);
}
if (week !== 0) {

   document.getElementById("seconds").value = Math.floor(week*60*60*24*7);
}
if (month !== 0) {

   document.getElementById("seconds").value = Math.floor(month*60*60*24*7*4.34534);
}
if (year !== 0) {

   document.getElementById("seconds").value = Math.floor(year*60*60*24*7*4.34524*12);
}   
}


function ConvertWeightToOther() {

let kg = Number( document.getElementById("kg").value );

  if (kg > 0 ) {

document.getElementById("gramm").value = Math.floor(kg*1000*100)/100;
document.getElementById("carat").value = Math.floor(kg*5000*100)/100;
document.getElementById("engpounds").value = Math.floor(kg*0.45359237*100)/100;
document.getElementById("pounds").value = Math.floor(kg*0.5*100)/100;
document.getElementById("stone").value = Math.floor(kg*0.157473*100)/100;
document.getElementById("ruspounds").value = Math.floor(kg*0.49*100)/100;

}
}
function OtherToWeight() {


let gramm = Number( document.getElementById("gramm").value );
let carat = Number( document.getElementById("carat").value );
let engpounds = Number( document.getElementById("engpounds").value );
let pounds = Number( document.getElementById("pounds").value );
let stone = Number( document.getElementById("stone").value );
let ruspounds = Number( document.getElementById("ruspounds").value );


if (gramm > 0) {
document.getElementById("kg").value = Math.floor(gramm/1000*100)/100; 
}
if (carat > 0) {
document.getElementById("kg").value = Math.floor(carat/5000*100)/100;
}
if (engpounds > 0) {
document.getElementById("kg").value = Math.floor(engpounds*0.45359237*100)/100;
}
if (pounds > 0) {
document.getElementById("kg").value = Math.floor(pounds*0.5*100)/100;
}
if (stone > 0) {
document.getElementById("kg").value = Math.floor(stone*0.157473*100)/100;
}
if (ruspounds > 0) {
document.getElementById("kg").value = Math.floor(ruspounds*0.49*100)/100;
}
}

function ConvertVolumeToOther() {

let litre = Number( document.getElementById("litre").value );

  if (litre > 0 ) {

document.getElementById("m3").value = Math.floor(litre/1000*100)/100;
document.getElementById("gallon").value = Math.floor(litre/3.785*100)/100;
document.getElementById("pint").value = Math.floor(litre*2.113*100)/100;
document.getElementById("quart").value = Math.floor(litre*1.057*100)/100;
document.getElementById("barrel").value = Math.floor(litre/159*100)/100;
document.getElementById("cubicfoot").value = Math.floor(litre/28.317*100)/100;
document.getElementById("cubicinch").value = Math.floor(litre/61.024*100)/100;

}
 
}
function ConvertOtherToVolume() {


let m3 = Number( document.getElementById("m3").value );
let gallon = Number( document.getElementById("gallon").value );
let pint = Number( document.getElementById("pint").value );
let quart = Number( document.getElementById("quart").value );
let barrel = Number( document.getElementById("barrel").value );
let cubicfoot = Number( document.getElementById("cubicfoot").value );
let cubicinch = Number( document.getElementById("cubicinch").value );


if (m3 > 0) {
document.getElementById("litre").value = Math.floor(m3*1000*100)/100;
}
if (gallon > 0) {
document.getElementById("litre").value = Math.floor(gallon*3.785*100)/100;
}
if (pint > 0) {
document.getElementById("litre").value = Math.floor(pint/2.113*100)/100;
}
if (quart > 0) {
document.getElementById("litre").value = Math.floor(quart/1.057*100)/100;
}
if (barrel > 0) {
document.getElementById("litre").value = Math.floor(barrel*159*100)/100;
}
if (cubicfoot > 0) {
document.getElementById("litre").value = Math.floor(cubicfoot*28.317*100)/100;
}
if (cubicinch > 0) {
document.getElementById("litre").value = Math.floor(cubicinch*61.024*100)/100;
}
}

function ConvertMToOther() {

let meter = Number( document.getElementById("meter").value );

  if (meter > 0 ) {

document.getElementById("km").value = Math.floor(meter/1000*100)/100;
document.getElementById("mile").value = Math.floor(meter*0.000621371*10000)/10000;
document.getElementById("nauticalmile").value = Math.floor(meter*0.000539957*10000)/10000;
document.getElementById("cable").value = Math.floor(meter*0.005396*10000)/10000;
document.getElementById("league").value = Math.floor(meter*0.000179986*10000)/10000;
document.getElementById("foot").value = Math.floor(meter*3.281*100)/100;
document.getElementById("yard").value = Math.floor(meter*1.094*100)/100;

}

}
function ConvertOtherToM() {


let km = Number( document.getElementById("km").value );
let mile = Number( document.getElementById("mile").value );
let nauticalmile = Number( document.getElementById("nauticalmile").value );
let cable = Number( document.getElementById("cable").value );
let league = Number( document.getElementById("league").value );
let foot = Number( document.getElementById("foot").value );
let yard = Number( document.getElementById("yard").value );


if (km > 0) {
document.getElementById("meter").value = Math.floor(km*1000*10)/10;
}
if (mile > 0) {
document.getElementById("meter").value = Math.floor(mile/0.000621371*10)/10;
}
if (nauticalmile > 0) {
document.getElementById("meter").value = Math.floor(nauticalmile/0.000539957*10)/10;
}
if (cable > 0) {
document.getElementById("meter").value = Math.floor(cable/0.005396*10)/10;
}
if (league > 0) {
document.getElementById("meter").value = Math.floor(league/0.000179986*10)/10;
}
if (foot > 0) {
document.getElementById("meter").value = Math.floor(foot/3.281*10)/10;
}
if (yard > 0) {
document.getElementById("meter").value = Math.floor(yard/1.094*10)/10;
}
}

function ConvertTempToOther() {

let c = Number( document.getElementById("c").value );

  if (c > 0 ) {

document.getElementById("k").value = Math.floor(c+273.15*100)/100;
document.getElementById("f").value = Math.floor((c*9/5)+32*100)/100;
document.getElementById("re").value = Math.floor(c*0.8*100)/100;
document.getElementById("ro").value = Math.floor(c*7.525*100)/100;
document.getElementById("ra").value = Math.floor(c*494.47*100)/100;
document.getElementById("n").value = Math.floor(c/3.030303*100)/100;
document.getElementById("d").value = Math.floor(c*148.5*100)/100;

}

}
function ConvertOtherToTemp() {


let k = Number( document.getElementById("k").value );
let f = Number( document.getElementById("f").value );
let re = Number( document.getElementById("re").value );
let ro = Number( document.getElementById("ro").value );
let ra = Number( document.getElementById("ra").value );
let n = Number( document.getElementById("n").value );
let d = Number( document.getElementById("d").value );


if (k > 0) {
document.getElementById("c").value = Math.floor(k-273.15*100)/100;
}
if (f > 0) {
document.getElementById("c").value = Math.floor((f/9/5)+32*100)/100;
}
if (re > 0) {
document.getElementById("c").value = Math.floor(re/0.8*100)/100;
}
if (ro > 0) {
document.getElementById("c").value = Math.floor(ro/7.525*100)/100;
}
if (ra > 0) {c
document.getElementById("c").value = Math.floor(ra/494.47*100)/100;
}
if (n > 0) {
document.getElementById("c").value = Math.floor(n*3.030303*100)/100;
}
if (d > 0) {
document.getElementById("c").value = Math.floor(d/148.5*100)/100;

}
}